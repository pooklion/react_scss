import React from 'react'

export default function tours() {
  return (
    <section class="section-tours">
                <div class="u-center-text u-margin-botttom-big">
                    <h2 class="heading-secondary">Our most popular packages</h2>
                </div>
                <div class="row">
                    <div class="col-1-of-3">
                        <div class="card">
                            <div class="card__side card__side--front">
                                <div class="card__picture card__picture-1">
                                    &nbsp;
                                </div>
                                <h4 class="card__heading">
                                    <span class="card__heading-span card__heading-span--1">Thai Food Package</span>
                                </h4>
                                <div class="card__details">
                                    <ul>
                                        <li>Tom Yum</li>
                                        <li>Pad Thai</li>
                                        <li>Pad See Ew</li>
                                        <li>Gang Ped</li>
                                        <li>Yum Woon Sen</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="card__side card__side--back card__side--back-1">
                                <div class="card__cta">
                                    <div class="card__price-box">
                                        <p class="card__price-only">Only</p>
                                        <p class="card__price-value">$297</p>
                                    </div>
                                    <a href="#popup" class="btn btn--white">Book now!</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-1-of-3">
                        <div class="card">
                            <div class="card__side card__side--front">
                                <div class="card__picture card__picture-2">
                                    &nbsp;
                                </div>
                                <h4 class="card__heading">
                                    <span class="card__heading-span card__heading-span--2">Indian food package</span>
                                </h4>
                                <div class="card__details">
                                    <ul>
                                        <li>Chana Masala</li>
                                        <li>Lemon Rice</li>
                                        <li>Rajma</li>
                                        <li>Paratha</li>
                                        <li>Jeera Aloo</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="card__side card__side--back card__side--back-2">
                                <div class="card__cta">
                                    <div class="card__price-box">
                                        <p class="card__price-only">Only</p>
                                        <p class="card__price-value">$297</p>
                                    </div>
                                    <a href="#popup" class="btn btn--white">Book now!</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-1-of-3">
                        <div class="card">
                            <div class="card__side card__side--front">
                                <div class="card__picture card__picture-3">
                                    &nbsp;
                                </div>
                                <h4 class="card__heading">
                                    <span class="card__heading-span card__heading-span--3">Other dishes</span>
                                </h4>
                                <div class="card__details">
                                    <ul>
                                        <li>Mexican Lasagna</li>
                                        <li>Gumbo Soup</li>
                                        <li>Grilled Artichokes</li>
                                        <li>Steak</li>
                                        <li>Pancakes</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="card__side card__side--back card__side--back-3">
                                <div class="card__cta">
                                    <div class="card__price-box">
                                        <p class="card__price-only">Only</p>
                                        <p class="card__price-value">$297</p>
                                    </div>
                                    <a href="#popup" class="btn btn--white">Book now!</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="u-center-text u-margin-top-huge">
                    <a href="#" class="btn btn--red">Discover all packages</a>    
                </div>
            </section>
  )
}
