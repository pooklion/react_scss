import React from 'react'

export default function navigation() {
  return (
    <div className="navigation">
        <input type="checkbox" className="navigation__checkbox" id="navi-toggle" />
        <label htmlFor="navi-toggle" className="navigation__button">
          <span className="navigation__icon" />
        </label>
        <div className="navigation__background">&nbsp;</div>
        <nav className="navigation__nav">
          <ul className="navigation__list">
            <li className="navigation__item"><a href="#" className="navigation__link">xxx</a></li>
            <li className="navigation__item"><a href="#" className="navigation__link">xxx</a></li>
            <li className="navigation__item"><a href="#" className="navigation__link">xxx</a></li>
            <li className="navigation__item"><a href="#" className="navigation__link">xxx</a></li>
            <li className="navigation__item"><a href="#" className="navigation__link">xxx</a></li>
          </ul>
        </nav>
      </div>
  )
}
