import React from 'react';

export default function book() {
  return (
    <section className="section-book">
        <div className="row">
          <div className="book">
            <div className="book__form">
              <form action="#" className="form">
                <div className="u-center-text u-margin-botttom-medium">
                  <h2 className="heading-secondary">Start Booking now!</h2>
                </div>
                <div className="form__group">
                  <input type="text" className="form__input" placeholder="Full name" id="name" required />
                  <label htmlFor="name" className="form__label">Full name</label>
                </div>
                <div className="form__group">
                  <input type="email" className="form__input" placeholder="Email address" id="email" required />
                  <label htmlFor="email" className="form__label">Email address</label>
                </div>
                <div className="form__group u-margin-botttom-medium">
                  <div className="form__radio-group">
                    <input type="radio" className="form__radio-input" id="thai" name="package" />
                    <label htmlFor="thai" className="form__radio-label">
                      <span className="form__radio-button" />
                      Thai foods
                    </label>
                  </div>
                  <div className="form__radio-group">
                    <input type="radio" className="form__radio-input" id="indian" name="package" />
                    <label htmlFor="indian" className="form__radio-label">
                      <span className="form__radio-button" />
                      Indian foods
                    </label>
                  </div>
                </div>
                <div className="form__group">
                  <button className="btn btn--blue">Next Step →</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </section>
  )
}
