import React from 'react';
import food1Large from '../assets/img/food1_large.jpg';
import food2Large from '../assets/img/food2_large.jpg';

export default function popup() {
  return (
    <div className="popup" id="popup">
        <div className="popup__content">
          <div className="popup__left">
            <img src={food1Large} alt="food" className="popup__img" />
            <img src={food2Large} alt="food" className="popup__img" />
          </div>
          <div className="popup__right">
            <a href="#section-tours" className="popup__close">×</a>
            <h2 className="heading-secondary u-margin-botttom-small">Start booking now</h2>
            <h3 className="heading-tertiary u-margin-botttom-small">Important – Please read these terms before booking</h3>
            <p className="popup__text">HTML is great for declaring static documents, but it falters when we try to use it for declaring dynamic views in web-applications. AngularJS lets you extend HTML vocabulary for your application. The resulting environment is extraordinarily expressive, readable, and quick to develop.</p>
            <a href="#" className="btn btn--blue">Book now!</a>
          </div>
        </div>
      </div>
  )
}
