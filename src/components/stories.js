import React from 'react';
import person1 from '../assets/img/person1.jpg'
import person2 from '../assets/img/person2.jpg'
import webmVideo from '../assets/img/Indian-Pedestrian.webm'
import mp4Video from '../assets/img/Indian-Pedestrian.mp4'

export default function stories() {
  return (
    <section className="section-stories">
        <div className="bg-video">
          <video className="bg-video__content" autoPlay muted loop>
            <source src={mp4Video} type="video/mp4" />
            <source src={webmVideo} type="video/webm" />
            Your browser is not supported!
          </video>
        </div>
        <div className="u-center-text u-margin-botttom-big">
          <h2 className="heading-secondary">Happy Students</h2>
        </div>
        <div className="row">
          <div className="story">
            <figure className="story__shape">
              <img src={person2} alt="student" className="story__image" />
              <figcaption className="story__caption">Jon Doe</figcaption>
            </figure>
            <div className="story__text">
              <h3 className="heading-tertiary u-margin-botttom-small">I had a best experience cooking thai foods</h3>
              <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptas nam quod voluptate voluptatum eaque? Numquam officiis sequi corporis, quia necessitatibus et magnam! Fugit voluptatibus magni, unde soluta pariatur ut ipsam!</p>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="story">
            <figure className="story__shape">
              <img src={person1} alt="student" className="story__image" />
              <figcaption className="story__caption">Jane Doe</figcaption>
            </figure>
            <div className="story__text">
              <h3 className="heading-tertiary u-margin-botttom-small">I had a best experience cooking thai foods</h3>
              <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptas nam quod voluptate voluptatum eaque? Numquam officiis sequi corporis, quia necessitatibus et magnam! Fugit voluptatibus magni, unde soluta pariatur ut ipsam!</p>
            </div>
          </div>
        </div>
        <div className="u-center-text u-margin-top-huge">
          <a href="#" className="btn-text">Learn more →</a>
        </div>
      </section>
  )
}
