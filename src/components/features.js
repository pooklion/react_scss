import React from 'react'

export default function features() {
  return (
    <section className="section-features">
        <div className="row">
          <div className="col-1-of-4">
            <div className="feature-box">
              <i className="feature-box__icon icon-basic-world" />
              <h3 className="heading-tertiary">Explore the menu</h3>
              <p className="feature-box__text">Lorem ipsum dolor sit amet consectetur adipisicing ipsum dolor sit amet consectetur ipsum dolor sit amet consecteturelit.</p>
            </div>
          </div>
          <div className="col-1-of-4">
            <div className="feature-box">
              <i className="feature-box__icon icon-basic-compass" />
              <h3 className="heading-tertiary">Flexible schedule</h3>
              <p className="feature-box__text">Lorem ipsum dolor sit amet consectetur adipisicing ipsum dolor sit amet consectetur ipsum dolor sit amet consecteturelit.</p>
            </div>
          </div>
          <div className="col-1-of-4">
            <div className="feature-box">
              <i className="feature-box__icon icon-basic-map" />
              <h3 className="heading-tertiary">Our locations</h3>
              <p className="feature-box__text">Lorem ipsum dolor sit amet consectetur adipisicing ipsum dolor sit amet consectetur ipsum dolor sit amet consecteturelit.</p>
            </div>
          </div>
          <div className="col-1-of-4">
            <div className="feature-box">
              <i className="feature-box__icon icon-basic-heart" />
              <h3 className="heading-tertiary">Tell your friends</h3>
              <p className="feature-box__text">Lorem ipsum dolor sit amet consectetur adipisicing ipsum dolor sit amet consectetur ipsum dolor sit amet consecteturelit.</p>
            </div>
          </div>
        </div>
      </section>
  )
}
