import React from 'react'
import Food1 from '../assets/img/food1.jpg';
import Food2 from '../assets/img/food2.jpg';
import Food3 from '../assets/img/food3.jpg';

export default function about() {
  return (
    <section className="section-about">
        <div className="u-center-text u-margin-botttom-big">
          <h2 className="heading-secondary">Cooking class in depth</h2>
        </div>
        <div className="row">
          <div className="col-1-of-2">
            <h3 className="heading-tertiary u-margin-bottom-small">You're going to fall in love with our food</h3>
            <p className="paragraph">Lorem ipsum dolor sit amet consectetur adipisicing elit. Hic, distinctio blanditiis minus est neque ducimus voluptas sint alias et sunt laboriosam architecto delectus amet reiciendis sequi. Natus quisquam necessitatibus at?</p>
            <h3 className="heading-tertiary u-margin-bottom-small">Live healthy like you never have before</h3>
            <p className="paragraph">Lorem ipsum dolor sit amet consectetur adipisicing elit. Hic, distinctio blanditiis minus est neque ducimus voluptas sint alias et sunt laboriosam architecto delectus amet reiciendis sequi. Natus quisquam necessitatibus at?</p>
            <a href="#" className="btn-text">Learn more →</a>
          </div>
          <div className="col-1-of-2">
            <div className="composition">
                <img src={Food1} alt="food1" className="composition__photo composition__photo--p1" />
                <img src={Food2} alt="food2" className="composition__photo composition__photo--p2" />
                <img src={Food3} alt="food4" className="composition__photo composition__photo--p3" />
            </div>
          </div>
        </div>
      </section>
  )
}
