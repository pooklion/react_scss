import React from 'react';
import logoMedium from '../assets/img/logo-medium.png'

export default function footer() {
  return (
    <footer className="footer">
        <div className="footer__logo-box">
          <picture className="footer__logo">
            <img src={logoMedium} alt="full logo" className="footer__logo" />
          </picture>
        </div>
        <div className="row">
          <div className="col-1-of-2">
            <div className="footer__navigation">
              <div className="footer__list">
                <li className="footer__item"><a href="#" className="footer__link">xxx</a></li>
                <li className="footer__item"><a href="#" className="footer__link">xxx</a></li>
                <li className="footer__item"><a href="#" className="footer__link">xxx</a></li>
                <li className="footer__item"><a href="#" className="footer__link">xxx</a></li>
                <li className="footer__item"><a href="#" className="footer__link">xxx</a></li>
              </div>
            </div>
          </div>
          <div className="col-1-of-2">
            <p className="footer__copyright">
              Design by JONAS SCHMEDTMANN<a href="#" className="footer__link">xxx</a>
            </p>
          </div>
        </div>
      </footer>
  )
}
