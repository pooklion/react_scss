import React from 'react';
import logoWhite from '../assets/img/logo-p-white-large.png'

const Header = () => {
  return (
    <div className="App">
      <header className="header">
        <div className="header__logo-box">
          <img src={logoWhite} alt="logo" className="header__logo" />
        </div>
        <div className="header__text-box">
          <h1 className="heading-primary">
            <span className="heading-primary--main">pookie</span>
            <span className="heading-primary--sub">learn to cook like a native</span>
          </h1>
          <a href="#" className="btn btn--white btn--animated">learn more</a>
        </div>
      </header>
    </div>
  );
}

export default Header;
