import React from 'react';
import logo from './logo.svg';
import './App.css';
import './sass/main.scss';
import Header from './components/header';
import About from './components/about';
import Features from './components/features';
import Tours from './components/tours';
import Stories from './components/stories';
import Book from './components/book';
import Footer from './components/footer';
import Navigation from './components/navigation';
import Popup from './components/popup';

function App() {
  return (
    <div className="App">
      <Navigation/>
      <Header/>
      <About/>
      <Features/>
      <Tours/>
      <Stories/>
      <Book/>
      <Footer/>
      <Popup/>
    </div>
  );
}

export default App;
